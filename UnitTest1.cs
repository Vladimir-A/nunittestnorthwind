using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace NUnitTestNorthwind1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver();
            driver.Url = "http://localhost:5000";
        }

        private void login()
        {
            driver.FindElement(By.Id("Name")).SendKeys("user");
            IWebElement passwordField = driver.FindElement(By.Id("Password"));
            passwordField.SendKeys("user");
            passwordField.Submit();
        }

        [Test]
        public void t01_TestLogin()
        {
            login();

            bool isLoginSuccessfull = false;
            try
            {
                IWebElement HomePageLabel = driver.FindElement(By.XPath(".//*[text()='Home page']"));
                isLoginSuccessfull = HomePageLabel.Displayed;
            }
            catch (NoSuchElementException)
            {
                isLoginSuccessfull = false;
            }
            driver.Close();
            Assert.IsTrue(isLoginSuccessfull, "Login failed");
        }

        [Test]
        public void t02_TestLogOut()
        {
            login();

            driver.FindElement(By.LinkText("Logout")).Click();
            bool Logout = false;
            try
            {
                IWebElement LoginPage = driver.FindElement(By.XPath(".//*[text()='Login']"));
                Logout = LoginPage.Displayed;
            }
            catch (NoSuchElementException)
            {
                Logout = false;
            }
            driver.Close();
            Assert.IsTrue(Logout, "Exit failed");
        }

        [Test]
        public void t03_TestAddNewProduct()
        {
            login();

            driver.FindElement(By.LinkText("All Products")).Click();
            driver.FindElement(By.LinkText("Create new")).Click();
            driver.FindElement(By.Id("ProductName")).SendKeys("testproduct");

            var Category = driver.FindElement(By.Id("CategoryId"));
            var selectCategory = new SelectElement(Category);
            selectCategory.SelectByText("Beverages");

            var Supplier = driver.FindElement(By.Id("SupplierId"));
            var selectSupplier = new SelectElement(Supplier);
            selectSupplier.SelectByText("Exotic Liquids");

            driver.FindElement(By.Id("UnitPrice")).SendKeys("10");
            driver.FindElement(By.Id("QuantityPerUnit")).SendKeys("10 boxes x 20 bags");
            driver.FindElement(By.Id("UnitsInStock")).SendKeys("5");
            driver.FindElement(By.Id("UnitsOnOrder")).SendKeys("2");
            driver.FindElement(By.Id("ReorderLevel")).SendKeys("3");
            driver.FindElement(By.XPath("//input[@class='btn btn-default']")).Click();

            bool AllProducts = false;
            try
            {
                IWebElement AllProductsPage = driver.FindElement(By.XPath(".//*[text()='All Products']"));
                AllProducts = AllProductsPage.Displayed;
            }
            catch (NoSuchElementException)
            {
                AllProducts = false;
            }
            driver.Close();
            Assert.IsTrue(AllProducts, "Exit failed");
        }

        [Test]
        public void t04_TestNewProductAndDelete()
        {
            login();

            driver.FindElement(By.LinkText("All Products")).Click();
            driver.FindElement(By.LinkText("testproduct")).Click();
            driver.FindElement(By.XPath("//input[@id='ProductName' and @value='testproduct']"));
            driver.FindElement(By.XPath("//select[@id='CategoryId']//option[contains(text(),'Beverages')]"));
            driver.FindElement(By.XPath("//select[@id='SupplierId']//option[contains(text(),'Exotic Liquids')]"));
            driver.FindElement(By.XPath("//input[@id='UnitPrice' and @value='10.0000']"));
            driver.FindElement(By.XPath("//input[@id='QuantityPerUnit' and @value='10 boxes x 20 bags']"));
            driver.FindElement(By.XPath("//input[@id='UnitsInStock' and @value='5']"));
            driver.FindElement(By.XPath("//input[@id='UnitsOnOrder' and @value='2']"));
            driver.FindElement(By.XPath("//input[@id='ReorderLevel' and @value='3']"));

            driver.FindElement(By.LinkText("All Products")).Click();
            driver.FindElement(By.XPath("//tr[79]//td[12]//a[1]")).Click();
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();

            bool AllProducts = false;
            try
            {
                IWebElement AllProductsPage = driver.FindElement(By.XPath(".//*[text()='All Products']"));
                AllProducts = AllProductsPage.Displayed;
            }
            catch (NoSuchElementException)
            {
                AllProducts = false;
            }
            driver.Close();
            Assert.IsTrue(AllProducts, "Exit failed");
        }

        public IWebDriver driver;
    }
}